package pl.edu.pwr.synchronization;

public class Singleton {

	private volatile static Singleton INSTANCE;
	private static int counter = 0;
	private int value;
	
	private Singleton() {
		this.value = ++counter;
		String threadName = Thread.currentThread().getName();
		System.out.println("Wątek " + threadName + " stworzył " + this.value + " singletona");
	}
	
	public static Singleton getInstance() {
		System.out.println("Wątek " + Thread.currentThread().getName() + " krok 1");
		if (INSTANCE == null) {
			System.out.println("Wątek " + Thread.currentThread().getName() + " krok 2");
			INSTANCE = new Singleton();
			System.out.println("Wątek " + Thread.currentThread().getName() + " krok 3");
		}
		return INSTANCE;
	}

	public int getValue() {
		return value;
	}
	
}
